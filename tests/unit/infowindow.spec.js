import InfoWindowMobile from '@/components/InfoWindowMobile.vue'

describe('InfoWindowMobile.vue', () => {
  it('contient les données par défaut', () => {
    expect(typeof InfoWindowMobile.data).toBe('function')
    const defaultData = InfoWindowMobile.data()
    expect(defaultData.titre).toBe(null)
  })
})
