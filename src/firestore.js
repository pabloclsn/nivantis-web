// import { db } from './main';
import firebase from 'firebase/app'
import 'firebase/firestore'
import {
  GeoFirestore
} from 'geofirestore'

const firebaseConfig = {
  apiKey: 'AIzaSyB0PYBMab_S8FpVsWgEeui53IrEGjG1huk',
  authDomain: 'niventis-web.firebaseapp.com',
  databaseURL: 'https://niventis-web.firebaseio.com',
  projectId: 'niventis-web',
  storageBucket: 'niventis-web.appspot.com',
  messagingSenderId: '924994634238',
  appId: '1:924994634238:web:2c670411e6e687e9'
}

firebase.initializeApp(firebaseConfig)

// Create a Firestore reference
const firestore = firebase.firestore()

// Create a GeoFirestore reference
const geofirestore = new GeoFirestore(firestore)

export function addDrugStore (name, adress, lat, lng) {
  // Add a document with a generated ID.
  const doc = {
    coordinates: new firebase.firestore.GeoPoint(lat, lng),
    name: name,
    adress: adress
  }

  geofirestore
    .collection('locations')
    .add(doc)
    .then(function (docRef) {
      console.log('Document written with ID: ', docRef.id)
    })
    .catch(function (error) {
      console.error('Error adding document: ', error)
    })
}

function fetchDrugFromSpecificDoc (docId) {
  return new Promise(function (resolve, reject) {
    let drugs = []
    firestore
      .collection('locations')
      .doc(docId)
      .collection('drugs_bought')
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (docu) {
          drugs.push({ name: docu.data().name, qty: docu.data().qty })
        })
        resolve(drugs)
      })
  })
}

async function checkIfDrugAlreadyExist (drugName) {
  let documentAlreadyExist = false
  return new Promise(function (resolve, reject) {
    firestore
      .collection('drugs')
      .where('name', '==', drugName)
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          console.log('Document exist', doc.data())
          documentAlreadyExist = true
        })
        resolve(documentAlreadyExist)
      })
  })
}

// drugsArray Example = [{name: Paracétamol}, {name: Doliprane}]
export function insertNewDrugs (drugsArray) {
  let anyError = false
  return new Promise(function (resolve, reject) {
    drugsArray.forEach(doc => {
      checkIfDrugAlreadyExist(doc.name).then(drugExist => {
        console.log(drugExist)

        if (!drugExist) {
          firestore
            .collection('drugs')
            .add(doc)
            .then(function (docRef) {
              console.log('New Drug_Document written with ID: ', docRef.id)
            })
            .catch(function (error) {
              console.error('Error adding document: ', error)
              anyError = true
            })
        }
      })
    })
    if (anyError) {
      // call to see if there is any error during insert
      reject(anyError)
    }
  })
}

// drugsArray Example = [{name: Paracétamol, qty: 100}, {name: Doliprane, qty: 100}]
export function insertDrugsInDrugsStore (docId, drugsArray) {
  let anyError = false
  return new Promise(function (resolve, reject) {
    drugsArray.forEach(drug => {
      let drugsBoughtDoc = {
        name: drug.name,
        qty: drug.qty
      }

      firestore
        .collection('locations')
        .doc(docId)
        .collection('drugs_bought')
        .add(drugsBoughtDoc)
        .then(function (docRef) {
          console.log('Drugs_bought_Document written with ID: ', docRef.id)
        })
        .catch(function (error) {
          console.error('Error adding Drugs_bought_Document: ', error)
          anyError = true
        })
    })
    if (anyError) {
      // call to see if there is any error during insert
      reject(anyError)
    }
  })
}

export function fetchDrugStoreNearUser () {
  // Create a GeoCollection reference
  return new Promise(function (resolve, reject) {
    const geocollection = geofirestore.collection('locations')

    // Create a GeoQuery based on a location
    const query = geocollection.near({
      center: new firebase.firestore.GeoPoint(47.211425, -1.56643),
      radius: 10
    })

    let markersArray = []
    // Get query (as Promise)
    query.get().then(value => {
      value.docs.forEach(doc => {
        let marker = {
          lat: doc.data().coordinates._lat,
          lng: doc.data().coordinates._long
        }
        fetchDrugFromSpecificDoc(doc.id).then(resolveDrugs => {
          let title = doc.data().name
          let address = doc.data().adress
          markersArray.push({
            title: title,
            position: marker,
            place: address,
            drugs: resolveDrugs
          })
        })
      })
      resolve(markersArray)
    })
  })
}
