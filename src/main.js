/* eslint-disable */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faMap, faCalculator, faStore } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import * as VueGoogleMaps from 'vue2-google-maps'
import VueFire from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'
import { addDrugStore,
  fetchDrugStoreNearUser,
  insertDrugsInDrugsStore,
  insertNewDrugs
  } from './firestore'
  
import { reject } from "q";

library.add(faMap)
library.add(faCalculator)
library.add(faStore)

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.config.productionTip = false;
Vue.use(VueFire);

//addDrugStore('Pharmacie du coin C', '10 rue du boie jolie', 47.286894, -1.587521)
//fetchDrugStoreNearUser()
let drugsArray = [{ name: "Paracétamol" }, { name: "Morpheus" }];
insertNewDrugs(drugsArray).then(anyError => {
  console.log(reject);
});


let drugsArrayWithQty = [{name: "Paracétamol", qty: 100}, {name: "Doliprane", qty: 100}]
insertDrugsInDrugsStore("VeCp7j0IzoGLnwCkqbog", drugsArrayWithQty)

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyDgzDKhFSqBiWRErMwFqbBSYG2Xza8tvmI",
    libraries: "places",
    autobindAllEvents: true,
    installComponents: true
  }
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
