import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Router from 'vue-router'
import Carte from './views/Carte.vue'
import Calculator from './views/Calculator.vue'
import Menu from './components/Menu.vue'
import NewStore from './views/NewStore.vue'

Vue.use(Router)
Vue.use(BootstrapVue)
export default new Router({
  routes: [
    {
      path: '/',
      name: 'Carte',
      component: Carte
    },
    {
      path: '/menu',
      name: 'Menu',
      component: Menu
    },
    {
      path: '/calculator',
      name: 'Calculatrice',
      component: Calculator
    },
    {
      path: '/newstore',
      name: 'NewStore',
      component: NewStore
    }
  ]
})
